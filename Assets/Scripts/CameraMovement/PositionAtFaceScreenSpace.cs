﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionAtFaceScreenSpace : MonoBehaviour
{
    [SerializeField]
    private float smoothTime = 0.2f;

    //Once stops, distance needed to move again
    [SerializeField]
    private float movementMinThreshold = 0.38f;

    [SerializeField]
    private float movementMaxThreshold = 0.5f;

    [SerializeField]
    private float stopDistance = 0.001f;

    [SerializeField]
    private int maxThresholdCountLimit = 10;

    //Valor entre pared x
    [SerializeField]
    private float MoveMultX = 10;

    //Valor entre pared Y
    [SerializeField]
    private float MoveMultY = 5.625f;

    private Vector3 velocity = Vector3.zero;
    private bool isLockedInPos = false;
    private int maxThresholdCount = 0;

    private void Update()
    {
        if (CameraFaceDetection.NormalizedFacePositions.Count != 0)
        {
            //transform.position = Camera.main.ViewportToWorldPoint(new Vector3(CameraFaceDetection.NormalizedFacePositions[0].x, CameraFaceDetection.NormalizedFacePositions[0].y, _camDistance));

            Vector3 finalPosition = new Vector3(
               (CameraFaceDetection.NormalizedFacePositions[0].position.x + CameraFaceDetection.NormalizedFacePositions[0].relativeWidth) * MoveMultX,
               (CameraFaceDetection.NormalizedFacePositions[0].position.y - (CameraFaceDetection.NormalizedFacePositions[0].relativeHeight)) * MoveMultY,
               this.transform.position.z);

            float sqrDistance = Vector3.SqrMagnitude(finalPosition - transform.position);

            if (sqrDistance < Mathf.Pow(movementMaxThreshold, 2) || maxThresholdCount >= maxThresholdCountLimit)
            {
                if (sqrDistance > Mathf.Pow(movementMinThreshold, 2) || !isLockedInPos)
                {
                    transform.position = Vector3.SmoothDamp(transform.position, finalPosition, ref velocity, smoothTime);
                    isLockedInPos = false;
                }
                else
                {
                    maxThresholdCount = 0;
                }

                if (Vector3.SqrMagnitude(finalPosition - transform.position) < stopDistance)
                    isLockedInPos = true;
            }
            else
            {
                maxThresholdCount++;
            }
        }
        else
        {
            maxThresholdCount = 0;
        }
    }
}