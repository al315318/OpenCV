﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShooting : MonoBehaviour
{
    [SerializeField]
    private ObjectPooler pool;

    [SerializeField]
    private Vector3 velocity;

    private bool canShoot;

    #region Getters & Setters

    public void SetCanShoot(bool canShoot)
    {
        this.canShoot = canShoot;
    }

    #endregion Getters & Setters

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space) && canShoot)
        //  Shoot();

        if (Input.GetButtonDown("Fire1") && canShoot)
            Shoot();
    }

    private void Shoot()
    {
        GameObject newGameObject = pool.GetPooledObject();
        newGameObject.transform.position = transform.position;
        newGameObject.transform.rotation = transform.rotation;

        newGameObject.GetComponent<Rigidbody>().velocity = velocity;
        newGameObject.SetActive(true);
    }
}