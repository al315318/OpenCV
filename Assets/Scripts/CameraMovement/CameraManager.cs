﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextTime
{
    public string text;
    public float time;

    public TextTime(string text, float time)
    {
        this.text = text;
        this.time = time;
    }
}

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    private ObjectSpawner objectSpawner;

    [SerializeField]
    private PointsGenerator pointsGenerator;

    [SerializeField]
    private CameraShooting cameraShooting;

    [SerializeField]
    private Canvas menuHack;

    [SerializeField]
    private Text canvasStartText;

    [SerializeField]
    private Text canvasCoinsText;

    [SerializeField]
    private Image canvasShootImage;

    [SerializeField]
    private Image numberBackImage;

    [SerializeField]
    private GameObject sceneMenu;

    [SerializeField]
    private int requiredCoins = 100;

    [SerializeField]
    private Text escText;

    [SerializeField]
    private Image escImage;

    private bool isInShootStage = false;
    private int coins = 0;
    private int defaultCoinsAddValue = 5;

    #region texts

    private List<TextTime> texts = new List<TextTime> {
        new TextTime("WELCOME TO THE GAME", 2),
        new TextTime("PRESS ESC KEY TO AVOID INTRODUCTION", 3),
        new TextTime("MOVE YOUR HEAD", 3),
        new TextTime("IF YOU HAVE ONE", 1),
        new TextTime("SURE YOU WANT TO PLAY", 2),
        new TextTime("BUT FIRST", 2),
        new TextTime("YOU NEED COINS", 2),
        new TextTime("SO", 1),
        new TextTime("SHOOT THE COINS", 2),
        new TextTime("TO GET 5 POINTS", 2),
        new TextTime("ALSO", 1),
        new TextTime("SHOOT THE MISSILES", 2),
        new TextTime("TO GET 1 POINT", 2),
        new TextTime("SHOOT THE COINS", 1),
        new TextTime("SHOOT THE MISSILES", 1),
        new TextTime("SHOOT THE COINS", 0.5f),
        new TextTime("SHOOT THE MISSILES", 0.5f),
        new TextTime("SHOOT THE COINS", 0.4f),
        new TextTime("SHOOT THE MISSILES", 0.4f),
        new TextTime("SHOOT THE COINS", 0.3f),
        new TextTime("SHOOT THE MISSILES", 0.3f),
        new TextTime("SHOOT THE COINS", 0.2f),
        new TextTime("SHOOT THE MISSILES", 0.2f),
        new TextTime("SHOOT THE COINS", 0.2f),
        new TextTime("SHOOT THE MISSILES", 0.2f),
        new TextTime("SHOOT WITH LEFT CLICK", 3),
        new TextTime("OR", 1.5f),
        new TextTime("SHOOT WITH X BUTTON", 3),
        new TextTime("SHOOT", 1f),
        new TextTime("SHOOT THEM", 1f),
        new TextTime("SHOOT THEM ALL", 1f),
        new TextTime("BUT REMEMBER", 2f),
        new TextTime("YOU NEED 50 COINS", 2f),
    };

    /*private List<TextTime> texts = new List<TextTime> {
    new TextTime("WELCOME TO THE GAME", 0.5f)};*/

    #endregion texts

    #region Getters & Setters

    public void AddPoints()
    {
        AddPoints(defaultCoinsAddValue);
    }

    public void AddPoints(int coins)
    {
        this.coins += coins;
        canvasCoinsText.text = "" + this.coins;

        if (this.coins >= requiredCoins)
            StartMenuStage();
    }

    #endregion Getters & Setters

    private void Awake()
    {
        if (objectSpawner == null)
            throw new Exception(gameObject.name + " necesita un object spawner");

        if (pointsGenerator == null)
            throw new Exception(gameObject.name + " necesita un points Generator");

        if (cameraShooting == null)
            throw new Exception(gameObject.name + " necesita un camera Shooting");

        if (canvasStartText == null)
            throw new Exception(gameObject.name + " necesita un canvas Start Text");

        if (canvasShootImage == null)
            throw new Exception(gameObject.name + " necesita un canvas Shoot Image");

        if (canvasCoinsText == null)
            throw new Exception(gameObject.name + " necesita un canvas Coins Text");

        if (numberBackImage == null)
            throw new Exception(gameObject.name + " necesita un number Back Image");

        if (escText == null)
            throw new Exception(gameObject.name + " necesita un escape text");

        if (escImage == null)
            throw new Exception(gameObject.name + " necesita un escape image");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StopAllCoroutines();
            if (isInShootStage)
                StartMenuStage();
            else
                StartShootStage();
        }
    }

    private void Start()
    {
        objectSpawner.enabled = false;
        numberBackImage.gameObject.SetActive(false);
        canvasStartText.gameObject.SetActive(true);
        canvasCoinsText.gameObject.SetActive(false);
        canvasShootImage.gameObject.SetActive(false);
        cameraShooting.SetCanShoot(false);
        pointsGenerator.gameObject.SetActive(false);
        sceneMenu.SetActive(false);
        menuHack.gameObject.SetActive(false);
        escText.gameObject.SetActive(true);
        escImage.gameObject.SetActive(true);

        objectSpawner.SetSpawnPoints(pointsGenerator.GetInstantiatedObjects());
        StartCoroutine(ChangeText());
    }

    private void StartShootStage()
    {
        objectSpawner.enabled = true;
        numberBackImage.gameObject.SetActive(true);
        canvasStartText.gameObject.SetActive(false);
        canvasCoinsText.gameObject.SetActive(true);
        canvasShootImage.gameObject.SetActive(true);
        cameraShooting.SetCanShoot(true);
        pointsGenerator.gameObject.SetActive(true);
        sceneMenu.SetActive(false);
        menuHack.gameObject.SetActive(false);
        escText.text = "Press esc to avoid mini-game";
        isInShootStage = true;
    }

    private void StartMenuStage()
    {
        objectSpawner.enabled = false;
        numberBackImage.gameObject.SetActive(false);
        canvasStartText.gameObject.SetActive(false);
        canvasCoinsText.gameObject.SetActive(false);
        canvasShootImage.gameObject.SetActive(true);
        cameraShooting.SetCanShoot(true);
        pointsGenerator.gameObject.SetActive(false);
        sceneMenu.SetActive(true);
        menuHack.gameObject.SetActive(true);
        escText.gameObject.SetActive(false);
        escImage.gameObject.SetActive(false);
    }

    private IEnumerator ChangeText()
    {
        foreach (var text in texts)
        {
            canvasStartText.text = text.text;
            yield return new WaitForSeconds(text.time);
        }

        StartShootStage();
    }
}