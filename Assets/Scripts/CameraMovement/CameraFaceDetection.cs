﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CameraFaceDetection : MonoBehaviour, ICameraProcess
{
    public static List<Face> NormalizedFacePositions { get; private set; }
    public static Vector2 CameraResolution;

    [SerializeField]
    private PositionAtFaceScreenSpace positionAtFaceScreenSpace;

    /// <summary>
    /// Downscale factor to speed up detection.
    /// </summary>
    private const int DetectionDownScale = 1;

    private bool _ready;
    private int _maxFaceDetectCount = 1;
    private CvCircle[] _faces;

    public void OnEnable()
    {
        int camWidth = 0, camHeight = 0;
        int result = OpenCVInterop.Init(ref camWidth, ref camHeight);
        if (result < 0)
        {
            if (result == -1)
            {
                Debug.LogWarningFormat("[{0}] Failed to find cascades definition.", GetType());
            }
            else if (result == -2)
            {
                Debug.LogWarningFormat("[{0}] Failed to open camera stream.", GetType());
            }

            return;
        }

        CameraResolution = new Vector2(camWidth, camHeight);
        _faces = new CvCircle[_maxFaceDetectCount];
        NormalizedFacePositions = new List<Face>();
        OpenCVInterop.SetScale(DetectionDownScale);
        _ready = true;
    }

    public void OnDisable()
    {
        if (_ready)
        {
            OpenCVInterop.Close();
        }
    }

    private void Update()
    {
        if (!_ready)
            return;

        int detectedFaceCount = 0;
        unsafe
        {
            fixed (CvCircle* outFaces = _faces)
            {
                OpenCVInterop.Detect(outFaces, _maxFaceDetectCount, ref detectedFaceCount);
            }
        }

        NormalizedFacePositions.Clear();
        for (int i = 0; i < detectedFaceCount; i++)
        {
            NormalizedFacePositions.Add(new Face(
                new Vector2((_faces[i].X * DetectionDownScale) / CameraResolution.x, 1f - ((_faces[i].Y * DetectionDownScale) / CameraResolution.y)),
                _faces[i].Radius / CameraResolution.x,
                _faces[i].Radius / CameraResolution.y));
        }
    }
}