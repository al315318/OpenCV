﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuTrigger : MonoBehaviour
{
    [SerializeField]
    private string scene;

    [SerializeField]
    private bool isPlayGame;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            if (!isPlayGame || (ScenesSaving.scenesSaving.characters > 0 && ScenesSaving.scenesSaving.layers > 0))
            {
                //Debug.Log("MENUTRIGGER");
                ScenesSaving.scenesSaving.LoadScene(scene);
            }
        }
    }

    public void HackMenuTrigger(string scene)
    {
        if (scene != "Platformer" || (ScenesSaving.scenesSaving.characters > 0 && ScenesSaving.scenesSaving.layers > 0))
            ScenesSaving.scenesSaving.LoadScene(scene);
    }

    public void HackMenuExit()
    {
        Application.Quit();
    }
}