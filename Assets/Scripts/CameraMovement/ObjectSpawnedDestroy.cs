﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectType
{
    coin, missile
}

public class ObjectSpawnedDestroy : ObjectDestroy
{
    [SerializeField]
    private ObjectType objectType;

    private CameraManager cameraManager;

    private ObjectSpawner objectSpawner;

    protected override void OnEnable()
    {
        Invoke("DestroySpawnedObject", destroyTime);
    }

    private void Start()
    {
        objectSpawner = GameObject.Find("ObjectSpawner").GetComponent<ObjectSpawner>();
        cameraManager = GameObject.Find("CameraManager").GetComponent<CameraManager>();
    }

    public void DestroySpawnedObject()
    {
        if (objectSpawner != null)
            objectSpawner.RemoveSpawnedObject(this);

        base.Destroy();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == triggerTag)
        {
            DestroySpawnedObject();

            switch (objectType)
            {
                case ObjectType.coin:
                    cameraManager.AddPoints();
                    break;

                case ObjectType.missile:
                    cameraManager.AddPoints(1);
                    break;

                default:
                    break;
            }
        }
    }
}