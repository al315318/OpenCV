﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField]
    private ObjectPooler pooler;

    [SerializeField]
    private float throwTime = 5f;

    [SerializeField]
    private GameObject spawnPoint;

    [SerializeField]
    private List<ObjectSpawnedDestroy> spawnedObjects = new List<ObjectSpawnedDestroy>();

    #region Getters & Setters

    public void SetPooler(ObjectPooler op)
    {
        pooler = op;
    }

    #endregion Getters & Setters

    private void OnEnable()
    {
        InvokeRepeating("ThrowObject", throwTime, throwTime);
    }

    private void ThrowObject()
    {
        GameObject newObject = pooler.GetPooledObject();
        if (newObject == null) return;
        newObject.transform.position = spawnPoint.transform.position;
        newObject.SetActive(true);

        ObjectSpawnedDestroy objectDestroy = newObject.GetComponent<ObjectSpawnedDestroy>();
        if (objectDestroy != null)
            spawnedObjects.Add(objectDestroy);
    }

    private void OnDisable()
    {
        Stop();
    }

    private void Stop()
    {
        CancelInvoke();

        foreach (var spawnedObject in spawnedObjects)
        {
            if (spawnedObject != null)
                spawnedObject.Destroy();
        }
        spawnedObjects.Clear();
    }
}