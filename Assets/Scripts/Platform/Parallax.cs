﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField]
    private List<Transform> backgrounds;				// Array of all the backgrounds to be parallaxed.

    [SerializeField]
    private float parallaxScale;					// The proportion of the camera's movement to move the backgrounds by.

    [SerializeField]
    private float parallaxReductionFactor;		// How much less each successive layer should parallax.

    [SerializeField]
    private float smoothing;						// How smooth the parallax effect should be.

    private Transform cam;						// Shorter reference to the main camera's transform.
    private Vector3 previousCamPos;				// The postion of the camera in the previous frame.

    #region Getters & Setters

    public void SetBackgrounds(List<Transform> backgrounds)
    {
        this.backgrounds = backgrounds;
    }

    #endregion Getters & Setters

    private void Awake()
    {
        // Setting up the reference shortcut.
        cam = Camera.main.transform;
    }

    private void Start()
    {
        if (backgrounds == null)
            throw new System.Exception(gameObject.name + " necesita un backgrounds");
        // The 'previous frame' had the current frame's camera position.
        previousCamPos = cam.position;
    }

    private void Update()
    {
        // The parallax is the opposite of the camera movement since the previous frame multiplied by the scale.
        float parallax = (previousCamPos.x - cam.position.x) * parallaxScale;

        // For each successive background...
        for (int i = 0; i < backgrounds.Count; i++)
        {
            // ... set a target x position which is their current position plus the parallax multiplied by the reduction.
            float backgroundTargetPosX = backgrounds[i].position.x + parallax * (i * parallaxReductionFactor + 1);

            // Create a target position which is the background's current position but with it's target x position.
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);

            // Lerp the background's position between itself and it's target position.
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }

        // Set the previousCamPos to the camera's position at the end of this frame.
        previousCamPos = cam.position;
    }
}