﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    [SerializeField]
    [Range(1, 4)]
    private int playerNumber = 1;

    [SerializeField]
    private PlatformSceneManager platformSceneManager;

    private bool hasCoin = false;

    private void Awake()
    {
        if (platformSceneManager == null)
            throw new System.Exception(gameObject.name + " necesita un platform Scene Manager");
    }

    #region Getters & Setters

    public bool GetHasCoin()
    {
        return hasCoin;
    }

    public void SetHasCoin(bool hasCoin)
    {
        this.hasCoin = hasCoin;
    }

    #endregion Getters & Setters

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (GetComponent<PlayerMovement>().GetCanMove())
        {
            if (other.tag == "End")
            {
                GetComponent<PlayerMovement>().SetCanMove(false);
                platformSceneManager.AddPlayerEnded(playerNumber, true);
            }

            if (other.tag == "Kill")
            {
                GetComponent<PlayerMovement>().SetCanMove(false);
                platformSceneManager.AddPlayerEnded(playerNumber, false);
                gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        hasCoin = false;
    }
}