﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float maxSpeed = 5f;

    [SerializeField]
    private float jumpForce = 1000f;

    [SerializeField]
    private Transform groundCheck;

    [SerializeField]
    private string jumpButton;

    [SerializeField]
    private string horizontalButton;

    [SerializeField]
    private bool canMove = true;

    private bool grounded = false;

    private Animation anim;					// Reference to the player's animator component.
    private bool facingRight = true;

    private bool jump = false;
    private Rigidbody2D rb;
    private Vector2 initialPos;

    #region Getters & Setters

    public void SetCanMove(bool canMove)
    {
        this.canMove = canMove;
    }

    public bool GetCanMove()
    {
        return canMove;
    }

    #endregion Getters & Setters

    private void Awake()
    {
        if (groundCheck == null)
            throw new System.Exception(gameObject.name + " necesita un ground check");
        rb = GetComponent<Rigidbody2D>();
        initialPos = transform.position;
        anim = GetComponent<Animation>();
    }

    private void OnEnable()
    {
        canMove = true;
    }

    private void OnDisable()
    {
        canMove = false;
        transform.position = initialPos;
    }

    private void Update()
    {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        // If the jump button is pressed and the player is grounded then the player should jump.
        if (Input.GetButtonDown(jumpButton) && grounded)
            jump = true;
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            // Cache the horizontal input.
            float h = Input.GetAxis(horizontalButton);

            // The Speed animator parameter is set to the absolute value of the horizontal input.
            //anim.SetFloat("Speed", Mathf.Abs(h));
            if (!Mathf.Approximately(h, 0)) anim.Play();
            else anim.Stop();

            rb.velocity = new Vector2(h * maxSpeed, rb.velocity.y);

            if (h > 0 && !facingRight)
                Flip();
            else if (h < 0 && facingRight)
                Flip();

            // If the player should jump...
            if (jump)
            {
                //anim.SetTrigger("Jump");
                //int i = Random.Range(0, jumpClips.Length);
                //AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);
                rb.AddForce(new Vector2(0f, jumpForce));
                jump = false;
            }
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}