﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpritesRenderers : MonoBehaviour
{
    public SpriteRenderer headSR;

    public SpriteRenderer torsoSR;

    public SpriteRenderer lArmSR;

    public SpriteRenderer rArmSR;

    public SpriteRenderer lLegSR;

    public SpriteRenderer rLegSR;

    private void Awake()
    {
        if (headSR == null)
            throw new System.Exception(gameObject.name + " necesita un head sprite renderer");
        if (torsoSR == null)
            throw new System.Exception(gameObject.name + " necesita un torso sprite renderer");
        if (lArmSR == null)
            throw new System.Exception(gameObject.name + " necesita un left arm sprite renderer");
        if (rArmSR == null)
            throw new System.Exception(gameObject.name + " necesita un right arm sprite renderer");
        if (lLegSR == null)
            throw new System.Exception(gameObject.name + " necesita un left leg sprite renderer");
        if (rLegSR == null)
            throw new System.Exception(gameObject.name + " necesita un right leg sprite renderer");
    }
}