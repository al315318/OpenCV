﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformSceneManager : MonoBehaviour
{
    [SerializeField]
    private PlatformCreator platformCreator;

    [SerializeField]
    private Parallax parallax;

    [SerializeField]
    private CameraFollowPlayers cameraFollowPlayers;

    [SerializeField]
    private List<GameObject> players;

    [SerializeField]
    private Button createPlatformButton;

    [SerializeField]
    private Image playerInfoImage;

    [SerializeField]
    private Image platformInfoImage;

    [SerializeField]
    private SpriteRenderer gridTemplate;

    [SerializeField]
    private Image platformImage;

    [SerializeField]
    private Text playerText;

    [SerializeField]
    private Text platformText;

    [SerializeField]
    private Button exitButton;

    [SerializeField]
    private Text player1PointsText;

    [SerializeField]
    private Text player2PointsText;

    [SerializeField]
    private Text player3PointsText;

    [SerializeField]
    private Text player4PointsText;

    [SerializeField]
    private Vector3 backgroundScaleMult;

    [SerializeField]
    private int platformMaxNumber = 2;

    [SerializeField]
    private float newRoundTime = 5f;

    [SerializeField]
    private int pointsToWin = 100;

    [SerializeField]
    private int pointsGoal = 10;

    [SerializeField]
    private int pointsAlone = 5;

    [SerializeField]
    private int pointsFirst = 3;

    [SerializeField]
    private int pointsCoin = 2;

    private int playerTurn = 1;
    private int sceneryLayerCount = 3;
    private Vector3 sceneryPosition = new Vector3(0, 0);
    private List<Transform> layers = new List<Transform>();
    private int platformNumber = 1;
    private int playerEnded = 0;
    private Transform newPlatform;
    private int playerCount = 4;

    private List<int> playersArrivedGoal = new List<int>();
    private List<int> playersPoints;
    private List<Text> playersPointsTexts;

    #region Getters & Setters

    public void SetSceneryLayerCount(int sceneryLayerCount)
    {
        this.sceneryLayerCount = sceneryLayerCount;
    }

    #endregion Getters & Setters

    private void Awake()
    {
        if (platformCreator == null)
            throw new System.Exception(gameObject.name + " necesita un platform Creator");
        if (cameraFollowPlayers == null)
            throw new System.Exception(gameObject.name + " necesita un camera Follow Players");
        if (parallax == null)
            throw new System.Exception(gameObject.name + " necesita un parallax");
        if (createPlatformButton == null)
            throw new System.Exception(gameObject.name + " necesita un create Platform Button");
        if (platformImage == null)
            throw new System.Exception(gameObject.name + " necesita un platform Image");
        if (playerText == null)
            throw new System.Exception(gameObject.name + " necesita un player Text");
        if (platformText == null)
            throw new System.Exception(gameObject.name + " necesita un platform Text");
        if (exitButton == null)
            throw new System.Exception(gameObject.name + " necesita un exit Button");
        if (player1PointsText == null)
            throw new System.Exception(gameObject.name + " necesita un player 1 Points Text");
        if (player2PointsText == null)
            throw new System.Exception(gameObject.name + " necesita un player 2 Points Text");
        if (player3PointsText == null)
            throw new System.Exception(gameObject.name + " necesita un player 3 Points Text");
        if (player4PointsText == null)
            throw new System.Exception(gameObject.name + " necesita un player 4 Points Text");
        if (playerInfoImage == null)
            throw new System.Exception(gameObject.name + " necesita un player Info Image");
        if (gridTemplate == null)
            throw new System.Exception(gameObject.name + " necesita un grid Template");
        if (platformInfoImage == null)
            throw new System.Exception(gameObject.name + " necesita un platform Info Image");

        playersPoints = new List<int>() { 0, 0, 0, 0 };
        playersPointsTexts = new List<Text>() { player1PointsText, player2PointsText, player3PointsText, player4PointsText };
    }

    private void RemovePlayers()
    {
        if (playerCount < 4)
        {
            for (int i = playerCount; i < players.Count; i++)
            {
                Destroy(players[i]);
                Destroy(playersPointsTexts[i]);
            }

            players.RemoveRange(playerCount, players.Count - playerCount);
            playersPointsTexts.RemoveRange(playerCount, playersPointsTexts.Count - playerCount);
            playersPoints.RemoveRange(playerCount, playersPoints.Count - playerCount);
        }
    }

    private void Start()
    {
        if (players == null || players.Count == 0)
            throw new System.Exception(gameObject.name + " necesita una lista de players");

        playerCount = ScenesSaving.scenesSaving.characters;
        sceneryLayerCount = ScenesSaving.scenesSaving.layers;

        RemovePlayers();
        cameraFollowPlayers.SetTargets(players);

        LoadScenery();
        parallax.SetBackgrounds(layers);

        platformCreator.enabled = false;

        LoadPlayers();
        AssignValuesPlatform();
        ShowCreatePlatform(true);
        ShowPoints(false);
        StopPlayers(true);
    }

    private void LoadPlayers()
    {
        Texture2D tex;
        for (int i = 0; i < players.Count; i++)
        {
            PlayerSpritesRenderers playerSR = players[i].GetComponent<PlayerSpritesRenderers>();
            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "5.png");
            playerSR.headSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "2.png");
            playerSR.torsoSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "4.png");
            playerSR.lArmSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "3.png");
            playerSR.rArmSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "1.png");
            playerSR.lLegSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Characters/image" + (i + 1) + "0.png");
            playerSR.rLegSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }

    private void LoadScenery()
    {
        Texture2D tex;

        for (int i = 1; i <= sceneryLayerCount; i++)
        {
            tex = LoadExtension.LoadPNG("Assets/Resources/OpenCVSprites/Scenery/image" + i + "0.png");
            if (tex != null)
            {
                GameObject newLayer = new GameObject();
                newLayer.name = "Layer" + i;
                newLayer.transform.position = sceneryPosition;
                SpriteRenderer newLayerSR = newLayer.AddComponent<SpriteRenderer>();
                newLayerSR.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                newLayerSR.sortingLayerName = "Back";
                newLayerSR.sortingOrder = i;
                newLayerSR.transform.localScale = backgroundScaleMult;
                layers.Add(newLayer.transform);
            }
        }
    }

    private void ShowCreatePlatform(bool value)
    {
        platformInfoImage.gameObject.SetActive(value);
        gridTemplate.gameObject.SetActive(value);
        createPlatformButton.gameObject.SetActive(value);
        platformImage.gameObject.SetActive(value);
        platformText.gameObject.SetActive(value);
        playerText.gameObject.SetActive(value);
        exitButton.gameObject.SetActive(value);
    }

    private void StopPlayers(bool value)
    {
        foreach (var player in players)
        {
            player.SetActive(!value);
        }
    }

    private void AssignValuesPlatform()
    {
        newPlatform = platformCreator.GetRndPlatform();
        platformImage.sprite = newPlatform.GetComponent<SpriteRenderer>().sprite;
        string platformName = newPlatform.gameObject.name;
        platformNumber = Random.Range(1, platformMaxNumber);
        playerText.text = "Player " + playerTurn;
        platformText.text = "Put " + platformNumber + " " + platformName;
    }

    public void CreatePlatforms()
    {
        platformCreator.enabled = true;
        platformCreator.GetPlatform(platformNumber, newPlatform.gameObject);
        platformCreator.enabled = false;
        createPlatformButton.gameObject.SetActive(false);
        //platformCreator.CreateMap();
    }

    public void PlatformsCreated()
    {
        playerTurn++;
        if (playerTurn > playerCount)
        {
            ShowCreatePlatform(false);
            StopPlayers(false);
        }
        else
        {
            AssignValuesPlatform();
            createPlatformButton.gameObject.SetActive(true);
        }
    }

    public void PlatformsFailure()
    {
        createPlatformButton.gameObject.SetActive(true);
    }

    public void AddPlayerEnded(int player, bool arriveGoal)
    {
        playerEnded++;
        if (arriveGoal)
            playersArrivedGoal.Add(player);
        if (playerEnded == playerCount)
        {
            ShowPoints(true);
            StopPlayers(true);
            AssignValuesPoints();
        }
    }

    private void NewRound()
    {
        playerEnded = 0;
        playerTurn = 1;
        playersArrivedGoal.Clear();
        ShowPoints(false);
        StopPlayers(true);
        AssignValuesPlatform();
        ShowCreatePlatform(true);
    }

    private void ShowPoints(bool value)
    {
        foreach (var playerPoints in playersPointsTexts)
        {
            playerPoints.gameObject.SetActive(value);
        }
        playerInfoImage.gameObject.SetActive(value);
    }

    private void AssignValuesPoints()
    {
        bool invoke = true;

        for (int i = 0; i < playersArrivedGoal.Count; i++)
        {
            int points = pointsGoal;
            if (playersArrivedGoal.Count == 0)
                points += pointsAlone;
            if (i == 0)
                points += pointsFirst;
            if (players[playersArrivedGoal[i] - 1].GetComponent<PlayerInfo>().GetHasCoin())
                points += pointsCoin;
            Debug.Log(playersPoints[playersArrivedGoal[i] - 1]);
            playersPoints[playersArrivedGoal[i] - 1] += points;
            playersPointsTexts[playersArrivedGoal[i] - 1].text = "Player " + (playersArrivedGoal[i]) + ": " + playersPoints[playersArrivedGoal[i] - 1];

            if (playersPoints[playersArrivedGoal[i] - 1] >= pointsToWin)
            {
                invoke = false;
                playersPointsTexts[i].text += " WINNER";
            }
        }

        if (invoke)
            Invoke("NewRound", newRoundTime);
        else
            exitButton.gameObject.SetActive(true);
    }

    public void GoToMenu()
    {
        ScenesSaving.scenesSaving.LoadScene("TecnicaCara");
    }
}