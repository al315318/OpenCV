﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class PlatformCreator : MonoBehaviour, ICameraProcess
{
    [SerializeField]
    private Transform wood;

    [SerializeField]
    private Transform empty;

    [SerializeField]
    private Transform spiky;

    [SerializeField]
    private Transform cannon;

    [SerializeField]
    private Transform coin;

    [SerializeField]
    private int maxAttempts = 50;

    [SerializeField]
    private ObjectPooler pooler;

    public const string black = "X";

    private GameObject[,] sourceMap = new GameObject[9, 7];
    //private Vector2 sourceMapSize = new Vector2(9, 7);

    [SerializeField]
    private UnityEvent triggerSuccess;

    [SerializeField]
    private UnityEvent triggerFailure;

    private List<Transform> platforms;

    private bool ready = false;
    private bool successProcess = false;

    #region Getters & Setters

    public Transform GetRndPlatform()
    {
        if (platforms.Count == 0)
            return null;
        return platforms[Random.Range(0, platforms.Count)];
    }

    #endregion Getters & Setters

    public void OnEnable()
    {
        int result = OpenCVInterop.InitGridDetection();

        if (result == -1)
        {
            throw new System.Exception(GetType() + " failed to open camera stream");
        }
        ready = true;
    }

    public void OnDisable()
    {
        if (ready)
        {
            OpenCVInterop.CloseGridDetection();
        }
    }

    public void Awake()
    {
        platforms = new List<Transform>() { wood, spiky, cannon, coin };
        //sourceMap = new GameObject[(int)sourceMapSize.x][];
    }

    public void CreateMap(GameObject newPlatform)
    {
        foreach (var child in GetComponentsInChildren<Transform>())
        {
            if (child.gameObject != this.gameObject)
                Destroy(child.gameObject);
        }

        string[][] openCVMap = readFile("Assets/Resources/Maps/CVMap.txt");
        // create planes based on matrix
        for (int y = 0; y < openCVMap.Length; y++)
        {
            for (int x = 0; x < openCVMap[0].Length; x++)
            {
                Debug.Log(x + " : x, y : " + y);

                if (openCVMap[y][x] == black)
                {
                    sourceMap[x, y] = newPlatform;
                }
            }
        }
        ShowMap();
        //triggerSuccess.Invoke();
    }

    private void ShowMap()
    {
        for (int x = 0; x < sourceMap.GetLength(0); x += 1)
        {
            for (int y = 0; y < sourceMap.GetLength(1); y += 1)
            {
                if (sourceMap[x, y] != null)
                {
                    GameObject newTile = (GameObject)Instantiate(sourceMap[x, y], new Vector3(transform.position.x + x, transform.position.y - y, 0), Quaternion.identity, this.transform);
                    newTile.name = "" + x + " ; " + y;
                    newTile.GetComponent<SpriteRenderer>().sortingLayerName = "Platform";
                    BulletSpawner os = newTile.GetComponent<BulletSpawner>();
                    if (os != null)
                        os.SetPooler(pooler);
                }
            }
        }
    }

    public void GetPlatform(int platformAmount, GameObject newPlatform)
    {
        if (ready)
        {
            ready = false;
            successProcess = false;
            StartCoroutine(GetPlatformAsynchronous(platformAmount, newPlatform));
        }
    }

    public IEnumerator GetPlatformAsynchronous(int amount, GameObject newPlatform)
    {
        int result = -1;
        int attempts = 0;

        while (attempts < maxAttempts && result == -1)
        {
            yield return null;

            result = OpenCVInterop.GetNewGrid(amount);

            Debug.Log("Ciclo " + attempts + ", result: " + result);
            attempts++;
        }

        if (result == 0)
        {
            CreateMap(newPlatform);
            triggerSuccess.Invoke();
        }
        else
        {
            triggerFailure.Invoke();
        }
        ready = true;
        yield break;
    }

    private string[][] readFile(string file)
    {
        string text = System.IO.File.ReadAllText(file);
        string[] lines = Regex.Split(text, "\r\n");
        int rows = lines.Length;

        string[][] levelBase = new string[rows][];
        for (int i = 0; i < lines.Length; i++)
        {
            string[] stringsOfLine = Regex.Split(lines[i], " ");
            levelBase[i] = stringsOfLine;
        }
        return levelBase;
    }
}