﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    [SerializeField]
    private Vector2 velocity;

    private void OnEnable()
    {
        GetComponent<Rigidbody2D>().velocity = velocity;
    }
}