﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class SpritesManager : MonoBehaviour, ICameraProcess
{
    [SerializeField]
    private int maxAttempts = 200;

    [SerializeField]
    protected int elementID = 1;

    [SerializeField]
    private UnityEvent successTrigger;

    [SerializeField]
    private UnityEvent failureTrigger;

    private bool ready = false;
    private bool successProcess = false;
    protected int nRects = 1;
    //private SpriteRenderer sr;

    protected List<SpriteRenderer> elementSR;
    protected List<string> imageFiles;

    #region Getters & Setters

    public void SetElementID(int elementID)
    {
        this.elementID = elementID;
        UpdatePathFiles();
        AssignValues();
    }

    public List<SpriteRenderer> GetElementSR()
    {
        return elementSR;
    }

    #endregion Getters & Setters

    protected virtual void Awake()
    {
        //sr = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;

        //transform.position = new Vector3(1.5f, 1.5f, 0.0f);

        UpdatePathFiles();

        AssignValues();
    }

    protected virtual void AssignValues()
    {
    }

    protected virtual void UpdatePathFiles()
    {
    }

    public void OnEnable()
    {
        int result = OpenCVInterop.InitElementDetection();

        if (result == -1)
        {
            throw new System.Exception(GetType() + " failed to open camera stream");
        }
        ready = true;
    }

    public void GetElement()
    {
        if (ready)
        {
            ready = false;
            successProcess = false;
            StartCoroutine(GetElementAsynchronous());
        }
    }

    public void OnDisable()
    {
        if (ready)
        {
            OpenCVInterop.CloseElementDetection();
        }
    }

    private IEnumerator GetElementAsynchronous()
    {
        int result = -1;
        int attempts = 0;
        //Debug.Log("Entrada");
        while (attempts < maxAttempts && result == -1)
        {
            yield return null;

            result = GetNewElement(elementID);

            //Debug.Log("Ciclo " + attempts + ", result: " + result);
            attempts++;
        }
        //Debug.Log("Salida");
        if (result == 0)
        {
            successProcess = CreateElementSprite(elementID);
            if (successProcess)
                successTrigger.Invoke();
            else
                failureTrigger.Invoke();
            //Close & open camera to avoid errors
            enabled = false;
            enabled = true;
        }
        else
            failureTrigger.Invoke();
        ready = true;
        yield break;
    }

    protected virtual int GetNewElement(int element)
    {
        return -1;
    }

    public bool CreateElementSprite(int characterID)
    {
        UpdatePathFiles();
        AssignValues();
        List<Sprite> sprites = new List<Sprite>();

        Texture2D tex;
        Debug.Log(imageFiles.Count);
        for (int i = 0; i < imageFiles.Count; i++)
        {
            tex = LoadExtension.LoadPNG(imageFiles[i]);
            Debug.Log(imageFiles[i]);
            if (tex == null)
                return false;
            sprites.Add(Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f));    //Razón entre proporciones: 1.2
        }

        //Para asedurarnos de quese han creado todos los sprites antes de asignarlos.
        for (int i = 0; i < sprites.Count; i++)
        {
            elementSR[i].sprite = sprites[i];
        }

        return true;
    }
}