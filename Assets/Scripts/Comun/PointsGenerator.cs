﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsGenerator : MonoBehaviour
{
    [SerializeField]
    private float width = 4;

    [SerializeField]
    private float height = 4;

    [SerializeField]
    private int resolutionWidth = 10;

    [SerializeField]
    private int resolutionHeight = 10;

    [SerializeField]
    private GameObject point;

    //0<attenuation<1 acentua, infinito->linea
    [SerializeField]
    private float attenuation = 2;

    private List<GameObject> instantiatedObjects = new List<GameObject>();

    #region Getters & Setters

    public List<GameObject> GetInstantiatedObjects()
    {
        return instantiatedObjects;
    }

    #endregion Getters & Setters

    private void Start()
    {
        if (point == null)
            throw new System.Exception(gameObject.name + " necesita un objeto que instanciar");

        GeneratePoints();
    }

    private void GeneratePoints()
    {
        Vector3 start = new Vector3(transform.position.x - (width / 2), transform.position.y - (height / 2), transform.position.z);

        for (int x = 0; x < resolutionWidth; x++)
        {
            for (int y = 0; y < resolutionHeight; y++)
            {
                //Encontrar un nuevo punto para el objeto

                float pointX = start.x;
                float pointY = start.y;
                float pointZ = start.z;

                if (resolutionWidth > 1)
                    pointX += (x * width / (resolutionWidth - 1));
                if (resolutionHeight > 1)
                    pointY += (y * height / (resolutionHeight - 1));
                if (Mathf.Approximately(attenuation, 0))
                    pointZ += Mathf.Abs(Mathf.Sqrt(Mathf.Pow(start.x, 2f) - Mathf.Pow(pointX - transform.position.x, 2f))) / attenuation;
                Vector3 nextPosition = new Vector3(pointX, pointY, pointZ);
                //Rotar un punto sobre un pivote
                nextPosition = transform.rotation * (nextPosition - transform.position) + transform.position;
                //Actualmente se usa la posicion original del objeto como suma a la posicion calculada.
                //Se usa la rotacion original del objeto
                GameObject instantiatedObject = Instantiate(point, nextPosition + point.transform.position, point.transform.rotation, transform);
                instantiatedObjects.Add(instantiatedObject);
            }
        }
    }
}