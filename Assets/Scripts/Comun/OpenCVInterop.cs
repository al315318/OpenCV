﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

// Define the structure to be sequential and with the correct byte size (3 ints = 4 bytes * 3 = 12 bytes)
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct CvCircle
{
    public int X, Y, Radius;
}

public class Face
{
    public Vector2 position;
    public float relativeWidth;
    public float relativeHeight;

    public Face(Vector2 position, float relativeWidth, float relativeHeight)
    {
        this.position = position;
        this.relativeWidth = relativeWidth;
        this.relativeHeight = relativeHeight;
    }
}

// Define the functions which can be called from the .dll.
internal static class OpenCVInterop
{
    [DllImport("PruebaVisionOpenCV")]
    internal static extern int Init(ref int outCameraWidth, ref int outCameraHeight);

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int Close();

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int SetScale(int downscale);

    [DllImport("PruebaVisionOpenCV")]
    internal unsafe static extern void Detect(CvCircle* outFaces, int maxOutFacesCount, ref int outDetectedFacesCount);

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int InitGridDetection();

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int CloseGridDetection();

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int GetNewGrid(int platformAmount);

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int GetNewCharacter(int character);

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int GetNewScenery(int scenery);

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int CloseElementDetection();

    [DllImport("PruebaVisionOpenCV")]
    internal static extern int InitElementDetection();
}