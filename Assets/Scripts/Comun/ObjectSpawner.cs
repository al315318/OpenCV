﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    //The last index is the bombs
    [SerializeField]
    private List<ObjectPooler> poolers;

    [SerializeField]
    private float throwTime = 5f;

    [SerializeField]
    private Vector3 velocity;

    [SerializeField]
    private List<GameObject> spawnPoints = new List<GameObject>();

    [SerializeField]
    private List<ObjectSpawnedDestroy> spawnedObjects = new List<ObjectSpawnedDestroy>();

    #region Getters & Setters

    public void SetSpawnPoints(List<GameObject> spawnPoints)
    {
        this.spawnPoints = spawnPoints;
    }

    public void RemoveSpawnedObject(ObjectSpawnedDestroy spawnedObject)
    {
        spawnedObjects.Remove(spawnedObject);
    }

    public void AddPooler(ObjectPooler op)
    {
        poolers.Add(op);
    }

    #endregion Getters & Setters

    private void OnEnable()
    {
        InvokeRepeating("ThrowObject", throwTime, throwTime);
    }

    private void ThrowObject()
    {
        int rndIndex;
        if (Random.Range(0, 4) == 0)
            rndIndex = poolers.Count - 1;
        else
            rndIndex = Random.Range(0, poolers.Count);

        GameObject newObject = poolers[rndIndex].GetPooledObject();
        if (newObject == null) return;
        Vector3 rndPos = spawnPoints[Random.Range(0, spawnPoints.Count)].transform.position;
        newObject.transform.position = rndPos;
        Rigidbody rb = newObject.GetComponent<Rigidbody>();
        if (rb != null)
            rb.velocity = velocity;
        newObject.SetActive(true);

        ObjectSpawnedDestroy objectDestroy = newObject.GetComponent<ObjectSpawnedDestroy>();
        if (objectDestroy != null)
            spawnedObjects.Add(objectDestroy);
    }

    private void OnDisable()
    {
        Stop();
    }

    private void Stop()
    {
        CancelInvoke();

        foreach (var spawnedObject in spawnedObjects)
        {
            if (spawnedObject != null)
                spawnedObject.Destroy();
        }
        spawnedObjects.Clear();
    }
}