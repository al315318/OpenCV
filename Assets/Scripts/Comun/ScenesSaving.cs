﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PlayerData
{
    public int characters;
    public int layers;
}

public class ScenesSaving : MonoBehaviour
{
    public static ScenesSaving scenesSaving;

    public int characters = 0;
    public int layers = 0;

    private void Awake()
    {
        if (scenesSaving == null)
        {
            DontDestroyOnLoad(gameObject);
            scenesSaving = this;
        }
        else if (scenesSaving != this)
        {
            Destroy(gameObject);
        }

        Load();
    }

    private void OnDestroy()
    {
        Save();
    }

    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    public void Save()
    {
        Debug.Log("saving");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData();
        data.characters = characters;
        data.layers = layers;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            characters = data.characters;
            layers = data.layers;
        }
    }
}