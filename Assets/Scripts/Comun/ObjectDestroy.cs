﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectDestroy : MonoBehaviour
{
    [SerializeField]
    protected float destroyTime = 5f;

    [SerializeField]
    protected string triggerTag;

    private void Awake()
    {
        if (triggerTag == null)
            throw new System.Exception(gameObject.name + " necesita un trigger tag");
    }

    protected virtual void OnEnable()
    {
        Invoke("Destroy", destroyTime);
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == triggerTag)
        {
            Destroy();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == triggerTag)
        {
            Destroy();
        }
    }
}