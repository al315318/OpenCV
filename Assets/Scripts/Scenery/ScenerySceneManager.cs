﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScenerySceneManager : MonoBehaviour
{
    [SerializeField]
    private GameObject scenery;

    [SerializeField]
    private SceneryManager sceneryManager;

    [SerializeField]
    private Button removeLayerButton;

    [SerializeField]
    private Button confirmSceneryButton;

    [SerializeField]
    private Image loadingImage;

    private List<SpriteRenderer> layersSR = new List<SpriteRenderer>();
    private System.IO.DirectoryInfo di = new DirectoryInfo("Assets/Resources/OpenCVSprites/Scenery");

    private void Awake()
    {
        if (scenery == null)
            throw new System.Exception(gameObject.name + " necesita un scenery gameobject");

        if (sceneryManager == null)
            throw new System.Exception(gameObject.name + " necesita un scenery Manager");

        if (removeLayerButton == null)
            throw new System.Exception(gameObject.name + " necesita un removeLayerButton");

        if (confirmSceneryButton == null)
            throw new System.Exception(gameObject.name + " necesita un confirmSceneryButton");

        if (loadingImage == null)
            throw new System.Exception(gameObject.name + " necesita un loadingImage");
    }

    private void Start()
    {
        removeLayerButton.gameObject.SetActive(false);
        confirmSceneryButton.gameObject.SetActive(false);
        loadingImage.gameObject.SetActive(false);

        foreach (System.IO.FileInfo file in di.GetFiles())
            file.Delete();
    }

    public void AddLayer()
    {
        if (layersSR.Count == 0 || layersSR[layersSR.Count - 1].sprite != null)
        {
            GameObject newLayer = new GameObject("S" + layersSR.Count);
            newLayer.GetComponent<Transform>().parent = scenery.transform;
            layersSR.Add(newLayer.AddComponent<SpriteRenderer>());
            newLayer.GetComponent<SpriteRenderer>().sortingOrder = layersSR.Count - 1;
            newLayer.transform.localScale = Vector3.one;
            newLayer.transform.localPosition = Vector3.one;
        }

        //Debug.Log(layersSR.Count);
        sceneryManager.SetScenerySR(layersSR[layersSR.Count - 1]);
        sceneryManager.SetElementID(layersSR.Count);
        sceneryManager.GetElement();
        loadingImage.gameObject.SetActive(true);
    }

    public void LayerAdded()
    {
        confirmSceneryButton.gameObject.SetActive(true);
        removeLayerButton.gameObject.SetActive(true);
        loadingImage.gameObject.SetActive(false);
    }

    public void LayerNotAdded()
    {
        loadingImage.gameObject.SetActive(false);
    }

    public void RemoveLayer()
    {
        Destroy(layersSR[layersSR.Count - 1].gameObject);
        layersSR.RemoveAt(layersSR.Count - 1);
        if (layersSR.Count == 0)
        {
            removeLayerButton.gameObject.SetActive(false);
            confirmSceneryButton.gameObject.SetActive(false);
        }
    }

    public void GoToMenu()
    {
        ScenesSaving.scenesSaving.LoadScene("TecnicaCara");
        ScenesSaving.scenesSaving.layers = layersSR.Count;
    }
}