﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneryManager : SpritesManager
{
    [SerializeField]
    private SpriteRenderer scenerySR;

    protected int nRects = 2;

    private string IMAGESCENERY;

    #region Getters & Setters

    public void SetScenerySR(SpriteRenderer scenerySR)
    {
        this.scenerySR = scenerySR;
    }

    #endregion Getters & Setters

    protected override void Awake()
    {
        //if (scenerySR == null)
        //throw new System.Exception(gameObject.name + " necesita un scenery sprite renderer");

        base.Awake();
    }

    protected override void AssignValues()
    {
        elementSR = new List<SpriteRenderer> { scenerySR };
        imageFiles = new List<string> { IMAGESCENERY };
    }

    protected override int GetNewElement(int element)
    {
        return OpenCVInterop.GetNewScenery(element);
    }

    protected override void UpdatePathFiles()
    {
        IMAGESCENERY = "Assets/Resources/OpenCVSprites/Scenery/image" + elementID + "0.png";
    }
}