﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSceneManager : MonoBehaviour
{
    [SerializeField]
    private Button addPlayerButton;

    [SerializeField]
    private Button confirmPlayersButton;

    [SerializeField]
    private Button removePlayersButton;

    [SerializeField]
    private Image loadingImage;

    [SerializeField]
    private CharacterManager characterManager;

    private int nextPlayer = 1;

    private void Awake()
    {
        if (addPlayerButton == null)
            throw new System.Exception(gameObject.name + " necesita un add Player Button");
        if (confirmPlayersButton == null)
            throw new System.Exception(gameObject.name + " necesita un confirm Players Button");
        if (removePlayersButton == null)
            throw new System.Exception(gameObject.name + " necesita un remove Players Button");
        if (characterManager == null)
            throw new System.Exception(gameObject.name + " necesita un character Manager");
        if (loadingImage == null)
            throw new System.Exception(gameObject.name + " necesita un loading Image");
    }

    private void Start()
    {
        addPlayerButton.gameObject.SetActive(true);
        confirmPlayersButton.gameObject.SetActive(false);
        removePlayersButton.gameObject.SetActive(false);
        loadingImage.gameObject.SetActive(false);
    }

    public void AddPlayer()
    {
        if (nextPlayer <= 4)
        {
            characterManager.GetElement();
            loadingImage.gameObject.SetActive(true);
        }
    }

    public void PlayerAdded()
    {
        Debug.Log("PLAYERADDED");
        loadingImage.gameObject.SetActive(false);
        characterManager.SetElementID(++nextPlayer);
        confirmPlayersButton.gameObject.SetActive(true);
        removePlayersButton.gameObject.SetActive(true);
        addPlayerButton.GetComponentInChildren<Text>().text = "Add Player " + nextPlayer;
        removePlayersButton.GetComponentInChildren<Text>().text = "Remove Player " + (nextPlayer - 1);
        if (nextPlayer > 4)
        {
            addPlayerButton.gameObject.SetActive(false);
        }
    }

    public void PlayerNotAdded()
    {
        loadingImage.gameObject.SetActive(false);
    }

    public void RemovePlayer()
    {
        if (nextPlayer > 1)
        {
            int previousPlayer = nextPlayer - 2;
            if (previousPlayer > 0)
            {
                characterManager.SetElementID(previousPlayer);
                characterManager.CreateElementSprite(previousPlayer);
            }
            else
            {
                foreach (var element in characterManager.GetElementSR())
                {
                    element.sprite = null;
                }
                confirmPlayersButton.gameObject.SetActive(false);
                removePlayersButton.gameObject.SetActive(false);
            }
        }
        characterManager.SetElementID(--nextPlayer);
        addPlayerButton.gameObject.SetActive(true);
        addPlayerButton.GetComponentInChildren<Text>().text = "Add Player " + nextPlayer;
        removePlayersButton.GetComponentInChildren<Text>().text = "Remove Player " + (nextPlayer - 1);
    }

    public void GoToMenu()
    {
        ScenesSaving.scenesSaving.LoadScene("TecnicaCara");
        ScenesSaving.scenesSaving.characters = nextPlayer - 1;
    }
}