﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : SpritesManager
{
    [SerializeField]
    private SpriteRenderer headSR;

    [SerializeField]
    private SpriteRenderer torsoSR;

    [SerializeField]
    private SpriteRenderer lArmSR;

    [SerializeField]
    private SpriteRenderer rArmSR;

    [SerializeField]
    private SpriteRenderer lLegSR;

    [SerializeField]
    private SpriteRenderer rLegSR;

    private string IMAGEHEAD;
    private string IMAGELARM;
    private string IMAGETORSO;
    private string IMAGERARM;
    private string IMAGELLEG;
    private string IMAGERLEG;

    protected int nRects = 7;

    protected override void Awake()
    {
        //sr = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;

        //transform.position = new Vector3(1.5f, 1.5f, 0.0f);

        if (headSR == null)
            throw new System.Exception(gameObject.name + " necesita un head sprite renderer");
        if (torsoSR == null)
            throw new System.Exception(gameObject.name + " necesita un torso sprite renderer");
        if (lArmSR == null)
            throw new System.Exception(gameObject.name + " necesita un left arm sprite renderer");
        if (rArmSR == null)
            throw new System.Exception(gameObject.name + " necesita un right arm sprite renderer");
        if (lLegSR == null)
            throw new System.Exception(gameObject.name + " necesita un left leg sprite renderer");
        if (rLegSR == null)
            throw new System.Exception(gameObject.name + " necesita un right leg sprite renderer");

        base.Awake();
    }

    protected override void AssignValues()
    {
        elementSR = new List<SpriteRenderer> { headSR, torsoSR, lArmSR, rArmSR, lLegSR, rLegSR };
        imageFiles = new List<string> { IMAGEHEAD, IMAGETORSO, IMAGELARM, IMAGERARM, IMAGELLEG, IMAGERLEG };
    }

    protected override int GetNewElement(int element)
    {
        return OpenCVInterop.GetNewCharacter(element);
    }

    protected override void UpdatePathFiles()
    {
        IMAGEHEAD = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "5.png";
        IMAGELARM = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "4.png";
        IMAGETORSO = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "2.png";
        IMAGERARM = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "3.png";
        IMAGELLEG = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "1.png";
        IMAGERLEG = "Assets/Resources/OpenCVSprites/Characters/image" + elementID + "0.png";
    }
}